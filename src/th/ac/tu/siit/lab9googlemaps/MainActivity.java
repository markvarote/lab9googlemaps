package th.ac.tu.siit.lab9googlemaps;

import java.util.Random;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends Activity {
	
	int CC = Color.RED;
	GoogleMap map;
	LocationManager locManager;
	Location currentLocation;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		MapFragment mapFragment = (MapFragment)getFragmentManager().findFragmentById(R.id.map);
		map = mapFragment.getMap();
		map.setMyLocationEnabled(true);//display a button to center the map at the current location
		
		/*MarkerOptions m1= new MarkerOptions();
		m1.position(new LatLng(0,0));
		map.addMarker(m1);
		
		MarkerOptions m2= new MarkerOptions();
		m2.position(new LatLng(13.75,100.4667));
		m2.title("Bangkok");
		map.addMarker(m2);
		
		PolylineOptions p= new PolylineOptions();
		p.add(new LatLng(13.75,100.4667));
		p.add(new LatLng(35.6895,139.6917));
		p.width(10);
		p.color(Color.BLUE);
		map.addPolyline(p);*/
		
		//map.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
		
		
		locManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
		currentLocation = locManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
		
		LocationListener locListener = new LocationListener() {
			@Override
			public void onLocationChanged(Location l) {
				PolylineOptions po1 = new PolylineOptions();
				po1.add(new LatLng(currentLocation.getLatitude(),currentLocation.getLongitude()));
				po1.add(new LatLng(l.getLatitude(),l.getLongitude()));
				po1.width(5);
				po1.color(CC);
				map.addPolyline(po1);
				
				currentLocation = l;
			}

			@Override
			public void onProviderDisabled(String provider) {
			}

			@Override
			public void onProviderEnabled(String provider) {
			}

			@Override
			public void onStatusChanged(String provider, int status,
					Bundle extras) {
			}
		};
		//5000 = minimum time
		//5 m = minimum distance
		locManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 5, locListener);
	}



	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	public int random(){
		Random r = new Random();
		float[] hsv = new float[3];
		hsv[1] = 1.0f;
		hsv[2] = 1.0f;
		hsv[0] = r.nextInt(360);
		int c = Color.HSVToColor(hsv);
		return c;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
		case R.id.mark:
			MarkerOptions mo = new MarkerOptions();
			mo.position(new LatLng(currentLocation.getLatitude(),currentLocation.getLongitude()));
			
			map.addMarker(mo);
			break;
		case R.id.color:
			CC= random();
			break;
		}
		return super.onOptionsItemSelected(item);
	}
}
